## SimTP Data Grid is a clustering and highly scalable data distribution platform.

Based on Hazelcast IMDG project, this fork of that project has a few modifications that are useful within the SimTP project 

### Features:

* Distributed implementations of `java.util.{Queue, Set, List, Map}`.
* Distributed implementation of `java.util.concurrent.locks.Lock`.
* Distributed implementation of `java.util.concurrent.ExecutorService`.
* Distributed `MultiMap` for one-to-many relationships.
* Distributed `Topic` for publish/subscribe messaging.
* Distributed Query, MapReduce and Aggregators.
* Synchronous (write-through) and asynchronous (write-behind) persistence.
* Transaction support.

